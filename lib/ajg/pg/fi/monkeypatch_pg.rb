
self.class.const_defined?(:PG) or
   raise "module PG not found (need to require 'pg'?)"

module PG
class Connection
   attr_reader :fi_cursor_cache
   def fi(i_options={})
      fi_cursor_cache.nil? and @fi_cursor_cache = {}
      AJG::PG::FI::FI.new(self,i_options)
   end
   def search_path
      t_user = current_user
      exec(%q{show search_path}).
         collect { |r|
            r.values.first.split(', ').collect { |n|
               n == '"$user"' ? t_user : n
            }
         }.first
   end
   def current_user
      exec(%q{select current_user}).first.values.first
   end
   def defined_schemas
      exec(%q{select schema_name from information_schema.schemata}).
         collect { |r| r.values.first }
   end
end
end # module PG

