
require_relative 'monkeypatch_pg'
require_relative 'decoders'

module AJG
module PG
module FI

class Error < Exception
end

class FI < BasicObject
   def initialize(i_conn,i_options)
      @conn    = i_conn
      @options = i_options

      @path = []
      @exec_flag = false
   end
   def method_missing(i_method,*i_args)
      @exec_flag and ::Kernel.raise "execution has started: #{i_method}"
      if i_args.count > 0 || i_method == :_run
         i_method == :_run or @path << i_method
         __exec__(i_args)
      else
         @path << i_method
         self
      end
   end
   def __exec__(i_args)
      @exec_flag = true
      @path.count == 0 and raise Error.new('no function path/name given')

      t_args = []
      t_named_flag = false
      while i_args.count > 0
         t_arg = i_args.shift
         if t_arg.kind_of?(::Hash)
            t_named_flag and
               raise Error.new("unable to understand args: #{i_args.inspect}")
            t_named_flag = true
            t_args << t_arg
         else
            t_named_flag and
               raise Error.new("unable to understand args: #{i_args.inspect}")
            t_args << t_arg
         end
      end

      t_sql = "#{__call_path__.join('.')}" +
         '(' +
         t_args.collect.with_index { |v,i|
            if v.kind_of?(::Hash)
               v.collect.with_index { |(n,_),ni|
                  "#{n} => $#{i + ni + 1}"
               }.join(',')
            else
               v.kind_of?(::Array) ? "$#{i+1}::#{v.last}" : "$#{i+1}"
            end
         }.join(',') +
         ')'

      t_bind_vals =
         t_args.collect.with_index { |v,i|
            if v.kind_of?(::Hash)
               v.collect { |_,nv| nv }
            else
               v.kind_of?(::Array) ? v.first : v
            end
         }.
         flatten
#         collect { |bv|
#            bv.__is_smart__ ? bv.__obj__ : bv
#         }

      t_ruby_fingerprint =
         __call_path__.join('.') + '(' +
            t_args.collect.with_index { |v,i|
               if v.kind_of?(::Hash)
                  v.collect.with_index { |(n,nv),ni|
                     "#{n} => #{nv.class}"
                  }.join(',')
               else
                  if v.kind_of?(::Array)
                     "#{v.first.class}::#{v.last}"
                  else
                     "#{v.class}"
                  end
               end
            }.join(',') +
            ')'

      t_sql = "select * from #{t_sql}"

#      $stderr.puts '-'*79
#      $stderr.puts "PATH: #{@path.inspect}"
#      $stderr.puts "INPUT_ARGS: #{i_args.inspect}"
#      $stderr.puts "CALL_PATH: #{__call_path__.inspect}"
#      $stderr.puts "ARGS: #{t_args.inspect}"
#      $stderr.puts "SQL: #{t_sql}"
#      $stderr.puts "BINDS: #{t_bind_vals.inspect}"
#      $stderr.puts "FINGERPRINT: #{t_ruby_fingerprint.inspect}"

      @conn.fi_cursor_cache.tap { |c|
         if !c.has_key?(t_ruby_fingerprint)
            c[t_ruby_fingerprint] = "fi-#{c.count}"
            @conn.prepare(c[t_ruby_fingerprint],t_sql)
         end
      }

      @conn.exec_prepared(
         @conn.fi_cursor_cache[t_ruby_fingerprint],
         t_bind_vals
      )
   end
   def __call_path__
      @call_path ||=
         case
            when @path.count == 1 then
               # is func()
               ["#{@path[0]}"]
            when @path.count == 2 then
               if @conn.defined_schemas.index(@path[0].to_s)
                  # is schema.func()
                  ["#{@path[0]}","#{@path[1]}"]
               else
                  # is package.func()
                  ["#{@path[0]}__#{@path[1]}"]
               end
            when @path.count == 3 then
               # is schema.packge.func()
               ["#{@path[0]}","#{@path[1]}__#{@path[2]}"]
            else
               raise Error.new("unable to understand: #{@path.inspect}")
         end
   end
end

end # module FI
end # module PG
end # module AJG

