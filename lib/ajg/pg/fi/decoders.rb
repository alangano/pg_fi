
class PG::TextDecoder::NULL < PG::SimpleDecoder
   def initialize
      super(oid:2278)
   end
   def decode(s,t,f)
      nil
   end
end
class PG::TextDecoder::RefCursor < PG::SimpleDecoder
   attr_accessor :conn
   def initialize(i_conn)
      super(conn: i_conn, oid:1790)
   end
   def decode(s,t,f)
      conn.exec(%Q{fetch all from "#{s}"})
   end
end
class PG::TextDecoder::Numeric < PG::SimpleDecoder
   def initialize
      super(oid:1700)
   end
   def decode(s,t,f)
      if s =~ /\A\.\Z/
         BigDecimal.new(s)
      else
         s.to_i
      end
   end
end
class PG::TextDecoder::Name < PG::SimpleDecoder
   def initialize
      super(oid:19)
   end
   def decode(s,t,f)
      s
   end
end

