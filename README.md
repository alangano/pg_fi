
# PG::FI -- Function Interface for Ruby

A ruby lib that gives method-call semantics to PostgreSQL stored-functions.

* uses (requires) the pg gem/library

# Examples

```
require 'pg'
require 'ajg/pg/fi'

t_conn = PG::Connection.new(<connection-info>)

# no parameter (needs ._run to trigger an execution)
t_conn.fi.database_timestamp._run   => <Time object>

# positional parameter
t_conn.fi.get_name_for_id(23)       => <some name>

# named parameter
t_conn.fi.get_name_for_id(i_id:23)    => <some name>

# mixed positional/named parameters
t_conn.fi.insert_contact_info(
   '<first-name>',
   '<last-name>',
   i_address1:   '<address>',
   i_city:       '<city>',
   i_state:      '<state>',
   i_zip:        '<zip>'
)                                => <contact-id>

# Pseudo-package (Oracle style, yo) Semantics
create or replace function workers__create(i_name IN varchar2) ...
create or replace function workers__start(i_id IN int8) ...
create or replace function workers__stop(i_id IN int8) ...
create or replace function workers__delete(i_id IN int8) ...
t_conn.fi.workers.create('<name>')     => <id>
t_conn.fi.workers.start('<id>')
t_conn.fi.workers.stop('<id>')
t_conn.fi.workers.delete('<id>')

# Specifying a Schema (from another schema)
t_conn.exec %q{set search_path = primary_app}
t_conn.fi.worker_app.workers.create('<name>')     => <id>
t_conn.fi.worker_app.workers.start('<id>')
t_conn.fi.worker_app.workers.stop('<id>')
t_conn.fi.worker_app.workers.delete('<id>')
```

* the test file test_fi.rb has working examples including handling refcursor return type

