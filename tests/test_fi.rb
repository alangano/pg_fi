#!/usr/bin/env ruby
$VERBOSE = true

require 'pathname'

$LOAD_PATH.unshift './lib'
require 'pg'
require 'ajg/pg/fi/fi'

# Some tweaks to PG type decoding for testing
class PG::TextDecoder::VOID < PG::SimpleDecoder
   def initialize
      super(oid:2278)
   end
   def decode(s,t,f)
      nil
   end
end
#class PG::TextDecoder::RefCursor < PG::SimpleDecoder
#   #attr_accessor :conn
#   def initialize(i_conn)
#      super(conn: i_conn, oid:1790)
#   end
#   def decode(s,t,f)
#      conn.exec(%Q{fetch all from "#{s}"})
#   end
#end
class PG::Result
   def array_all
      # all
      each_row.collect { |r| r }
   end
   def one
      # first column of first row
      array_all.first.first
   end
end



require 'test/unit'

class Test_File < Test::Unit::TestCase
   attr_reader :conn
   def setup
      @conn = PG::Connection.new(
         host:       'localhost',
         port:       6666,
         dbname:     'fi_test',
         user:       'fi_test',
         password:   'fi_test'
      )
      @conn.type_map_for_results = PG::BasicTypeMapForResults.new(@conn)
      @conn.type_map_for_results.add_coder(PG::TextDecoder::VOID.new)
      @conn.type_map_for_results.add_coder(
         PG::TextDecoder::RefCursor.new(@conn)
      )

      @conn.exec %q{set search_path = "$user",public}
   end
   def teardown
   end
   def test_null_func
      assert_nil conn.fi.null_func._run.one
   end
   def test_time_no_tz_result
      conn.fi.db_time._run.one.tap { |t|
         assert_kind_of Time,t
         assert_equal(
            '2016-07-22 10:11:34 -0700',  # session tz gets added by ruby
            t.strftime('%Y-%m-%d %H:%M:%S %z')
         )
      }
   end
   def test_1_pos_arg
      assert_equal 'TEST',conn.fi.upper_this('test').one
   end
   def test_1_named_arg
      assert_equal 'TEST',conn.fi.upper_this(i_input:'test').one
   end
   def test_2_pos_args
      assert_equal 7,conn.fi.add_these(3,4).one
   end
   def test_2_named_args
      assert_equal 7, conn.fi.add_these(i_val1:3,i_val2:4).one
   end
   def test_2_named_args_out_of_order
      assert_equal 7, conn.fi.add_these(i_val2:3,i_val1:4).one
   end
   def test_2_mixed_args
      assert_equal 7, conn.fi.add_these(3,i_val2:4).one
   end
   def test_specifying_public_schema
      assert_equal 'TEST',conn.fi.public.upper_this('test').one
      assert_equal 'TEST',conn.fi.public.upper_this(i_input:'test').one
      assert_equal 7,conn.fi.public.add_these(3,4).one
      assert_equal 7, conn.fi.public.add_these(i_val1:3,i_val2:4).one
      assert_equal 7, conn.fi.public.add_these(i_val2:3,i_val1:4).one
      assert_equal 7, conn.fi.public.add_these(3,i_val2:4).one
   end
   def test_package
      assert_nil conn.fi.package1.func1._run.one
      assert_nil conn.fi.public.package1.func1._run.one
   end
   def test_refcursor
      conn.transaction {
         # refcursor usage has to be within transaction

conn.fi.package1.func2._run.one.each_row { |r| puts r.inspect }

#         assert_equal(
#            [
#               ["information_schema"],
#               ["pg_catalog"],
#               ["public"],
#               ["supsup"],
#               ["whut"]
#            ],
#            conn.fi.package1.func2._run.one.array_all
#         )
         # NOTE: you call .one on the first PG::Result, which gives
         # you the refcursor as PG::Result, which you then operate on as
         # a normal cursor.
         # ALSO: this requires the PG::TextDecoder::RefCursor from above
      }
   end
   def test_overloaded_func
      # Hmm, PostgreSQL will default to the varchar input over the others
      assert_equal 'varchar', conn.fi.overloaded1(1).one
      assert_equal 'varchar', conn.fi.overloaded1(1.1).one
      assert_equal 'varchar', conn.fi.overloaded1('1').one

      # ... unless you tell it which you want by way of specifying datatype ...
      assert_equal 'int8', conn.fi.overloaded1([1,:int8]).one
      assert_equal 'float', conn.fi.overloaded1([1.1,:float]).one
      assert_equal 'varchar', conn.fi.overloaded1(['1',:varchar]).one

      # Same behavior in psql, so I don't think it's gem-pg
   end
   def test_1_out_variable
      assert_equal 'TEST', conn.fi.package1.func3('test').one
   end
   def test_2_out_variables
      assert_equal(
         [["TEST", "test-test"]],
         conn.fi.package1.func4('test').array_all
      )
   end
   def test_1_in_out_variable
      assert_equal 'whut-test', conn.fi.package1.func5('test','whut').one
   end
   def test_2_in_out_variables
      assert_equal(
         [["KCA-A", -1]],
         conn.fi.package1.func6('ack','kca',5).array_all
      )
   end
end

