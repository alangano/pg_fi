
create user fi_test encrypted password 'fi_test';
create database fi_test encoding='UTF8' owner=fi_test;

\connect fi_test - localhost

create schema whut;
create schema supsup;

-------------------------------------------------------------------------------
create or replace function public.null_func() returns void as
$$
begin
   null;
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.db_time(
) returns timestamp without time zone as
$$
begin
   return to_timestamp(
         '2016-07-22 10:11:34',
         'YYYY-MM-DD HH24:MI:SS'
      )::timestamp without time zone;
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.upper_this(i_input IN varchar) returns varchar as
$$
begin
   return upper(i_input);
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.add_these(
   i_val1   IN int8,
   i_val2   IN int4
) returns int8 as
$$
begin
   --raise notice 'i_val1: %', i_val1;
   --raise notice 'i_val2: %', i_val2;
   return i_val1 + i_val2;
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.overloaded1(
   i_val1      IN int8
) returns varchar as
$$
begin
   return 'int8';
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.overloaded1(
   i_val1      IN float
) returns varchar as
$$
begin
   return 'float';
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.overloaded1(
   i_val1      IN varchar
) returns varchar as
$$
begin
   return 'varchar';
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.package1__func1() returns void as
$$
begin
   null;
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.package1__func2() returns refcursor as
$$
declare
   t_cur    refcursor;
begin
   open t_cur for
      select schema_name from information_schema.schemata order by 1;
   return t_cur;
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.package1__func3(
   i_text      IN varchar,
   o_text      OUT varchar
) returns varchar as
$$
begin
   o_text := upper(i_text);
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.package1__func4(
   i_text      IN varchar,
   o_text1     OUT varchar,
   o_text2     OUT varchar
) returns record as
$$
begin
   o_text1 := upper(i_text);
   o_text2 := i_text || '-' || i_text;
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.package1__func5(
   i_text         IN varchar,
   io_text1       IN OUT varchar
) returns varchar as
$$
begin
   io_text1 := io_text1 || '-' || i_text;
end;
$$ language plpgsql;

-------------------------------------------------------------------------------
create or replace function public.package1__func6(
   i_text         IN varchar,
   io_text1       IN OUT varchar,
   io_int1        IN OUT int
) returns record as
$$
begin
   io_text1 := substr(upper(io_text1 || '-' || i_text),1,io_int1);
   io_int1 := -1;
end;
$$ language plpgsql;

-------------------------------------------------------------------------------



